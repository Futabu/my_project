#include <stdio.h> //importação de bibliotecas
#include "dice.h"

int main(){
	
	int faces;
	initializeSeed();
	printf("How many faces?\n");
	scanf("%d", &faces);
	printf("Let's roll the dice: %d\n", rollDice(faces));
	return 0;
}
//ITAndroids rules
